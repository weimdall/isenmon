/**
 * \file Joueur.cpp
 * \brief Gestion de l'utilisateur
 * \author G.Fitas
 */

#include "joueur.h"
using namespace std;
joueur::~joueur()
{
    delete m_isenmonActuel;
    for(int i=0;i<m_nbIsenmon-1;i++)
        delete m_listeIsenmon[i];
    //dtor
}
joueur::joueur(std::string nom)
{
    int id;
    m_nom = nom;
    m_ndf = datasave + nom;
    m_ndf = m_ndf + "/" + nom ;
    ifstream fileJoueur(m_ndf.c_str(),ios::in);
    if(fileJoueur)
    {
        string temp;
        getline(fileJoueur,temp);
        m_nbIsenmon = stoi(temp.c_str());
        for(int i=0;i<m_nbIsenmon;i++)
        {
            getline(fileJoueur,temp);
            id = stoi(temp.c_str());
            if(i==0)
                m_isenmonActuel = new isenmon(id,nom);
            else
                m_listeIsenmon.push_back(new isenmon(id,nom));
        }
        temp = datasave + nom + string("/listObjet");
        ifstream fileJoueurListeObjet(temp.c_str(),ios::in);
        while(!fileJoueurListeObjet.eof())
        {
            getline(fileJoueurListeObjet,temp);
            temp = dataobjet + temp;
            ifstream fileObjet(temp.c_str(),ios::in);
            if(fileObjet)
            {
                objet obj;
                getline(fileObjet,temp);
                obj.nom = temp;
                getline(fileObjet,temp);
                obj.attraper = stoi(temp.c_str());
                getline(fileObjet,temp);
                obj.attraperEfficacite = stoi(temp.c_str());
                getline(fileObjet,temp);
                obj.soins = stoi(temp.c_str());
                getline(fileObjet,temp);
                obj.soinsType = stoi(temp.c_str());
                bool exist = false;
                unsigned int i = 0;
                while((i<m_listeObjet.size())&&(exist==false))
                {
                    if(m_listeObjet[i].nom==obj.nom)
                        exist = true;
                    i++;
                }
                if(exist)
                    m_listeObjetNombre[i-1]++;
                else
                {
                    m_listeObjet.push_back(obj);
                    m_listeObjetNombre.push_back(1);
                }
            }
            else
                cout<<"Vous vous etes fait baiser cette objet n'existe pas XDD"<<endl;
        }
    }
    else
    {
        cout<<"Vous etes nouveau !!"<<endl;
        cout<<"Bienvenu "<<nom<<endl;
        fileJoueur.close();

        string temp = datasave + nom + string("/listIsenmon"); // creation du fichier listisenmon qui contiendra l'adresse des isenmon

        ofstream fileJoueurListeIsenmon(temp.c_str(),ios::trunc);

        fileJoueurListeIsenmon.close();

        temp = datasave + nom + string("/listObjet"); // creation du fichier listisenmon qui contiendra l'adresse des isenmon

        ofstream fileJoueurListeObjet(temp.c_str(),ios::trunc);

        ofstream fileJoueur(m_ndf.c_str(),ios::trunc);
        cout<<"Votre fichier sera ecrit ici : "<<m_ndf<<endl;
        fileJoueur<<"0";
        fileJoueur.close();
    }
}
std::string joueur::getNom()
{
    return m_nom;
}
std::string joueur::getNdf()
{
    return m_ndf;
}
isenmon* joueur::getIsenmonActuel()
{
    return m_isenmonActuel;
}
void joueur::sauver()
{
    m_isenmonActuel->sauver();
    for(int i=0;i<m_nbIsenmon-1;i++)
    {
        m_listeIsenmon[i]->sauver();
    }
}
vector<isenmon*> joueur::getListeIsenmon()
{
    return m_listeIsenmon;
}
int joueur::getNbIsenmon()
{
    return m_nbIsenmon;
}
void joueur::attraper(int id)
{
    if(id>0)
    {
        isenmon a(id);
        a.setPossedeur(m_nom);
        string ndf = datasave+m_nom+string("/isenmon/")+a.getNom();
        a.setNdf(ndf);
        a.sauver();
        string temp = datasave + m_nom + string("/listIsenmon");
        ofstream fileListeIsenmon(temp.c_str(),ios::out|ios::app);
        if(fileListeIsenmon)
        {
            temp = string("isenmon/")+a.getNom();
            fileListeIsenmon<<endl<<temp;
            fileListeIsenmon.close();
        }
        else
            cout<<"PAF DANS TA GUEULE 1"<<endl;

    }
}

void joueur::changerIsenmon(int id)
{
    if((m_nbIsenmon>=id) && (id>0))
    {
        isenmon *temp = new isenmon(1);
        temp = m_isenmonActuel;
        m_isenmonActuel = m_listeIsenmon[id];
        m_listeIsenmon[id] = temp;
    }
}
void joueur::afficherObjer(int id)
{
    cout<<"Le nom de l'objet est : "<<m_listeObjet[id].nom<<" et vous en avez "<<m_listeObjetNombre[id]<<endl;
    if(m_listeObjet[id].attraper)
    {
        cout<<"Cet objet permet d'attraper un isenmon"<<endl;
        cout<<"Cet objet a une efficacite de : "<<m_listeObjet[id].attraperEfficacite<<endl;
    }
    else
    {
        cout<<"Cet objet permet de soigner de : "<<m_listeObjet[id].soins<<endl;
        cout<<"Cet objet permet de soigner les isenmon "<<m_listeObjet[id].soinsType<<endl;
    }
}
void joueur::utiliserObjet(int id,isenmon &a)
{
    if(m_listeObjet[id].attraper)
    {
        if(((rand()%(2000)-1)/(a.getLevel()/20))>1000)
            attraper(a.getId());
    }
    else
    {
        if(a.getEtat()==m_listeObjet[id].soinsType)
            a.setEtat(0);
        a.ajouterPv(m_listeObjet[id].soins);
    }
    m_listeObjetNombre[id]--;
    if(m_listeObjetNombre[id]<=0)
    {
        m_listeObjet.erase(m_listeObjet.begin()+id);
        m_listeObjetNombre.erase(m_listeObjetNombre.begin()+id);
    }
}
