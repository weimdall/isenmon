/**
 * \file Personnage.h
 * \brief Gestion des personnages
 * \author J.Cassagne
 */


#ifndef PERSONNAGE_H
#define PERSONNAGE_H

#include "Animation.h"
#include <string>

class Personnage
{
    public:
        Personnage(std::string str);
        virtual ~Personnage();
        void Avancer(int direction);
        void Stop();
        void setPosition(sf::Vector2f vect);
        void setPosition(float x, float y);
        sf::Sprite* getSprite();
        sf::Vector2u getSize();

    private:
        Animation *anim[4];
        bool stop;
        sf::Sprite *sprPerso;
        int m_direction;

};

#endif // PERSONNAGE_H
