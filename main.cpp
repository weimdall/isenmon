#include "Programme.h"

int main()
{
    Programme prgm;
    int selection;
    do
    {
        selection = prgm.Menu();
        switch(selection)
        {
        case 0:
            prgm.Jeu(true);
            break;
        case 1:
            prgm.Jeu(false);
            break;
        case 2:
            break;
        case 3:
            break;
        default:
            selection = 3;
            break;
        }
    } while(selection != 3);

    return 0;
}
