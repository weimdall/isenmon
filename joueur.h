/**
 * \file Joueur.h
 * \brief Gestion de l'utilisateur
 * \author G.Fitas
 */


#include <string>
#include <time.h>
#include "isenmon.h"
#define datasave "./data/save/"
#define dataobjet "./data/objet/"
#ifndef JOUEUR_H
#define JOUEUR_H

struct objet
{
    std::string nom;
    int soins;
    int soinsType;
    bool attraper;
    int attraperEfficacite;
};
class joueur
{
    public:
        joueur(std::string);

        std::string getNom();
        std::string getNdf();
        isenmon* getIsenmonActuel();
        std::vector<isenmon*> getListeIsenmon();
        int getNbIsenmon();

        void attraper(int);

        void utiliserObjet(int,isenmon&);
        void sauver();
        void changerIsenmon(int);
        void afficherObjer(int);


        ~joueur();
    private:
        std::string m_nom;
        std::string m_ndf;

        isenmon * m_isenmonActuel;
        std::vector<isenmon*> m_listeIsenmon;
        int m_nbIsenmon;
        std::vector<objet> m_listeObjet;
        std::vector<int> m_listeObjetNombre;
};

#endif // JOUEUR_H
