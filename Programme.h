/**
 * \file Programme.h
 * \brief Classe principale
 * \author J.Cassagne
 */

#ifndef PROGRAMME_H
#define PROGRAMME_H

#define ZOOM 0.8

#define POLICE "./data/ressources/"
#define TEXTURE "./data/ressources/"
//#define MAPS "./data/ressources/maps"
//#define SON "./data/ressources/"
//#define ANIMATION dans Animation.h

#include <SFML/Graphics.hpp>
#include <vector>

#ifdef __linux__
#include <unistd.h>
#else
#include <windows.h>
#endif


#include "Animation.h"
#include "Personnage.h"
#include "Mapping.h"
#include "Evenements.h"
#include "Son.h"
#include "joueur.h"

class Programme
{
    public:
        Programme();
        ~Programme();

        int Menu();
        int Jeu(bool nouvellePartie);
        void Notification(std::string str);

        void DrawJeu();
        void DrawCombat();

    private:
        bool ChargementJeu();
        bool DechargementJeu();
        int Combat();
        void Clignoter(sf::Sprite *spr, unsigned int clignotement);

        void AttendreEntree();

        void BarreVie(float rapport, sf::VertexArray &barre);

        static int NbAleatoire(int a, int b);

        std::vector <sf::Drawable*>toDrawJeu;
        std::vector <sf::Drawable*>toDrawCombat;
        sf::RenderWindow *App;
        sf::View vue;
        sf::Font Fipps;
        sf::Vector2f posActuel;
        sf::Texture textObjectif;
        sf::Sprite sprObjectif;
        sf::Mutex mutex;
        Mapping *terrain;
        Evenements *evenements;
        Direction etat;
        Personnage *perso;
        Son *musique, *cle;
        joueur *player;
        isenmon *isen;

        sf::Clock clock;
};

#endif // PROGRAMME_H
