/**
 * \file pnj.cpp
 * \brief Gestion des PNJ
 * \author G.Fitas
 */


#include "pnj.h"


using namespace std;
pnj::pnj(string nom)
{
    m_nom=nom;
    m_ndf=DATAPNJ+nom;
    m_perso = new Personnage(m_nom);

    ifstream pnj(m_ndf.c_str(),ios::in);
    string temp;
    if(pnj)
    {
        getline(pnj,temp); // isenmon Actuel
        m_isenmonActuel = new isenmon(temp);
        getline(pnj,temp); // isenmon dans liste
        m_nbisenmon = atoi(temp);
        for(int i=0;i<atoi(temp);i++) // on rempli le vecteur m_listeIsenmon
        {
            getline(pnj,temp);
            m_listeIsenmon.push_back(new isenmon(temp));
        }
        getline(pnj,temp);
        m_pos_x = atoi(temp);
        getline(pnj,temp);
        m_pos_y = atoi(temp);
        getline(pnj,temp);
        m_porte = atoi(temp);
        m_perso->setPosition(m_pos_x,m_pos_y);
    }
    else
    {
        cout<<endl<<"Impossible d'acceder a : "<<m_ndf<<endl;
    }
    //ctor
}
std::string pnj::getNom()
{
    return  m_nom;
}
std::string pnj::getNdf()
{
    return m_ndf;
}
isenmon* pnj::getIsenmonActuel()
{
    return m_isenmonActuel;
}
std::vector<isenmon*> pnj::getListeIsenmon()
{
    return m_listeIsenmon;
}
int pnj::getNbIsenmon()
{
    return m_nbisenmon;
}

void pnj::changerIsenmon(int id)
{
    if((m_nbIsenmon>=id) && (id>0))
    {
        isenmon *temp = new isenmon(1);
        temp = m_isenmonActuel;
        m_isenmonActuel = m_listeIsenmon[id];
        m_listeIsenmon[id] = temp;
    }
}
void pnj::aller(float x,float y)
{
    m_perso->Avancer(2);
    //la je sais �s comment on avance et on tourne
}

pnj::~pnj()
{
    //dtor
}
