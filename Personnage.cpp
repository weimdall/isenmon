/**
 * \file Personnage.cpp
 * \brief Gestion des personnages
 * \author J.Cassagne
 */

#include "Personnage.h"

Personnage::Personnage(std::string str)
{
    stop = true;
    anim[0] = new Animation(str+"Haut.txt");
    anim[1] = new Animation(str+"Bas.txt");
    anim[2] = new Animation(str+"Droite.txt");
    anim[3] = new Animation(str+"Gauche.txt");
    sprPerso = anim[1]->getSprite();
    m_direction = 1;
}

Personnage::~Personnage()
{
    for(int i = 0 ; i < 4 ; i++)
        delete anim[i];
}

void Personnage::Avancer(int direction)
{
    stop = false;
    if((direction >= 0) && (direction <= 3))
        m_direction = direction;
}

void Personnage::Stop()
{
    stop = true;
    anim[m_direction]->setImageActuelle(1);
    sprPerso = anim[m_direction]->getSprite();
}

sf::Sprite* Personnage::getSprite()
{
    if(!stop)
        sprPerso = anim[m_direction]->getSprite();
    return sprPerso;
}

sf::Vector2u Personnage::getSize()
{
    return anim[m_direction]->getSize();
}

void Personnage::setPosition(sf::Vector2f vect)
{
    this->setPosition(vect.x, vect.y);
}
void Personnage::setPosition(float x, float y)
{
    /*x -= sprPerso->getGlobalBounds().width/2;
    y -= sprPerso->getGlobalBounds().height/2;*/
    sprPerso->setOrigin(sprPerso->getGlobalBounds().width/2, sprPerso->getGlobalBounds().height/2);
    sprPerso->setPosition(x, y);
}
