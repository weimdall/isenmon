/**
 * \file isenmon.h
 * \brief Gestion des isenmons
 * \author G.Fitas
 */

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <stdlib.h>
#include "Son.h"
#include "Animation.h"
#ifndef ISENMON_H
#define ISENMON_H

#define datasave "./data/save/"
#define data "./data/action/"

std::string itos(int );
int stoi(std::string );
struct action
{
    std::string nom;
    std::string description;

    int attaque;
    int soins;

    int etatAttaquant;
    int etatAttaque;

    int type;


    int nombreAttaquePossibleMax;

    Son *musiqueAnim;
    Animation *anim;
};

class isenmon
{
    public:
        isenmon(int id,std::string ); // creer un nouveau isenmon appartenant au joueur de type string
        isenmon(int id); // creer un nouveau isenmon pour ceux qui apparaissent aleatoirement

        //methode get
        std::string getNom();
        std::string getNdf();
        std::string getPossedeur();
        bool getEnVie();
        int getId();
        int getType();
        int getVieActuel();
        int getVie();
        int getLevel();
        int getEtat();
        Son *getMusiqueAnim();
        Animation *getAnim();
        int getEtatTourRestant();
        std::vector<std::string> getStringNomActionPossible();
        std::vector<action> getActionPossible();
        std::vector<int> getNombreAttaquePossible();

        //methode set
        void setType(int);
        void setVieActuel(int);
        void setVie(int);
        void setLevel(int);
        void setEtat(int );
        void setEtatTourRestant(int);
        void setNdf(std::string);
        void setPossedeur(std::string);
        void setNombreAttaquePossible(int,int);

        void attaque(int,isenmon&);

        void cpy(isenmon);

        void enleverPv(int);
        void ajouterPv(int);

        void tourSuivant();

        void affiche();

        void sauver();

        ~isenmon();
    private:
        std::string m_nom; // nom de l'isenmon
        std::string m_ndf; // emplacement du fichier
        std::string m_possedeur;
        int m_id; // id de l'isenmon
        int m_type; // type de l'isenmon
        int m_typeTemp; // type de l'isenmon temporaire
        int m_vieActuel; // vie actuel de l'isenmon
        int m_vie; // vie maximum de l'isenmon
        int m_level; // niveau actuel de l'isenmon
        int m_etat; // etat de l'isenmon
        int m_etatTourRestant; // nombre de tour restant a l'etat
        std::vector<action> m_actionPossible; // action possible                action
        std::vector<std::string> m_stringNomActionPossible; //action possible   string
        std::vector<int> m_nombreAttaquePossible;
        Son *m_musiqueAnim;
        Animation *m_anim;
};

#endif // ISENMON_H
