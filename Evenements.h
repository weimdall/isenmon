/**
 * \file Evenements.h
 * \brief Gestion des evenements
 * \author J.Cassagne
 */

#ifndef EVENEMENTS_H
#define EVENEMENTS_H

#include <vector>
#include <fstream>
#include <iostream>
#include <SFML/Graphics.hpp>

class Evenements
{
    public:
        Evenements();
        virtual ~Evenements();

        bool LireFichier(std::string str);
        bool TestObjectif(int x, int y); //X et Y = nb de Tile, retourne true si un message doit etre affich�
        std::vector<std::string> Intro(); //Retourne un tableau contenant toutes l'intro (1 page = 1 indice du tableau)
        std::string Message(int x, int y); //Retourne un message (si il y a lieu) � afficher
        bool ObjectifAtteint();

    private:
        std::vector <sf::Vector2f>position; //Tableau des position correspondant au messages devant �tre afficher
        std::vector <std::string>message; //tableau des messages
        std::vector <unsigned int>listeIndicePopup; //contient la liste des indices des messages � n'afficher qu'une fois, ils seront ensuite supprim�
        std::vector <std::string>intro; //Tableau contenant l'intro
        sf::Vector2f posObjectif; //Position de l'objectif
        bool objectif;
};

#endif // EVENEMENTS_H
