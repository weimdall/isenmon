/**
 * \file Evenements.cpp
 * \brief Gestion des evenements
 * \author J.Cassagne
 */

#include "Evenements.h"

Evenements::Evenements()
{
    //ctor
}

Evenements::~Evenements()
{
    //dtor
}

bool Evenements::LireFichier(std::string str)
{
    std::string chaine;
    int type;
    objectif = false;
    float x, y;
    std::ifstream fichier(str.c_str(), std::ios::in);

    fichier >> chaine;
    if(chaine != "Event-Version-1.0")
        return false;
    while(type != -1)
    {
        fichier >> type;
        switch(type)
        {
        case 1:
            std::getline(fichier,chaine);
            std::getline(fichier,chaine);
            intro.push_back(chaine);
            break;
        case 2:
            fichier >> x >> y;
            std::getline(fichier,chaine);
            std::getline(fichier,chaine);
            position.push_back(sf::Vector2f(x, y));
            message.push_back(chaine);
            break;
        case 3:
            fichier >> x >> y;
            std::getline(fichier,chaine);
            std::getline(fichier,chaine);
            position.push_back(sf::Vector2f(x, y));
            message.push_back(chaine);
            listeIndicePopup.push_back(message.size()-1);
            break;
        case 4:
            fichier >> x >> y;
            posObjectif.x = x;
            posObjectif.y = y;
            break;
        default:
            break;
        }
    }
    for(unsigned int i = 0 ; i < intro.size() ; i++)
    {
        for(unsigned int j = 0 ; j < intro[i].size()-1 ; j++)
        {
            if(intro[i][j] == '\\' && intro[i][j+1] == 'n')
                intro[i].replace(j, 2, "\n");
        }
    }
    for(unsigned int i = 0 ; i < message.size() ; i++)
    {
        for(unsigned int j = 0 ; j < message[i].size()-1 ; j++)
        {
            if(message[i][j] == '\\' && message[i][j+1] == 'n')
                message[i].replace(j, 2, "\n");
        }
    }
    return true;
}

std::vector<std::string> Evenements::Intro()
{
    return intro;
}

bool Evenements::TestObjectif(int x, int y)
{
    if((posObjectif.x == x) && (posObjectif.y == y) && (!objectif))
    {
        objectif = true;
        return true;
    }
    return false;
}

std::string Evenements::Message(int x, int y)
{
    bool supr = false;
    std::string chaine;
    for(unsigned int i = 0 ; i < position.size() ; i++)
    {
        if((position[i].x == x) && (position[i].y == y))
        {
            for(unsigned int j = 0 ; j < listeIndicePopup.size() ; j++)
            {
                if(listeIndicePopup[j] == i)
                {
                    supr = true;
                    listeIndicePopup.erase(listeIndicePopup.begin()+j);
                    for(unsigned int k = j ; k < listeIndicePopup.size() ; k++)
                        listeIndicePopup[k]--;
                }
            }
            chaine = message[i];
            if(supr)
            {
                position.erase(position.begin()+i);
                message.erase(message.begin()+i);
            }
            return chaine;
        }
    }
    return "";
}

bool Evenements::ObjectifAtteint()
{
    return objectif;
}
