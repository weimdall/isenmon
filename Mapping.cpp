
/**
 * \file Mapping.cpp
 * \brief Gestion du TileMapping
 * \author J.Cassagne
 */


#include "Mapping.h"
#include <iostream>

Mapping::Mapping(sf::RenderWindow *App)
{
    //Initialisation des param�tres par d�faut
    fen = App;
    m_nbTileHauteur = 0;
    m_nbTileLargeur = 0;
    m_resolutionHauteur = 0;
    m_resolutionLargeur = 0;
    m_nbBlocsLargeur = 0;
    m_nbBlocsHauteur = 0;
    calqueCollision = 0;
}

Mapping::~Mapping()
{
   multiCalques.clear();
}

bool Mapping::LireFichier(std::string nomFichier)
{
    std::string chaine;
    int param[4];
    int RVB[3];
    std::ifstream fichier(nomFichier.c_str(), std::ios::in);
    fichier >> chaine; //On recupere la premi�re ligne
    if(chaine != "Tilemapping-Version-1.0")
        return false;
    fichier >> chaine; //On recupere le chemin vers l'image
    if(!m_ref.loadFromFile(MAPS+chaine))
        return false;

    //Extraction des param�tres de la map
    fichier >> param[0] >> param[1] >> param[2] >> param[3];
    m_nbTileLargeur = param[0];
    m_nbTileHauteur = param[1];
    m_resolutionLargeur = param[2];
    m_resolutionHauteur = param[3];

    fichier >> param[0] >> param[1];
    m_nbBlocsLargeur = param[0];
    m_nbBlocsHauteur = param[1];

    //Lecture de la couleur � rendre transparent
    fichier >> RVB[0] >> RVB[1] >> RVB[2];
    m_ref.createMaskFromColor(sf::Color(RVB[0], RVB[1], RVB[2]));
    m_refText.loadFromImage(m_ref);
    m_refText.setSmooth(false);
    m_refText.setRepeated(false);

    //Lecture du lieu de spawn
    fichier >> param[0] >> param[1];
    spawn.x = m_resolutionLargeur*param[0];
    spawn.y = m_resolutionHauteur*param[1];

    //Calque collison
    fichier >> param[0];
    this->calqueCollision = param[0]-1;

    //liste des blocs vide (pour les collision)
    fichier >> param[0];
    while(param[0] != -1)
    {
        m_vide.push_back(param[0]-1);
        fichier >> param[0];
    }

    //enregistrement de la matrice et des differents calques
    std::vector <Tile>tblTemp;
    std::vector <std::vector <Tile> >calque;
    do
    {
        for(int i = 0 ; i < m_nbBlocsHauteur ; i++)
        {
            for(int j = 0 ; j < m_nbBlocsLargeur ; j++)
            {
                Tile tileTemp;
                fichier >> tileTemp.bloc;
                tileTemp.bloc--;
                tblTemp.push_back(tileTemp);
            }
            calque.push_back(tblTemp);
            tblTemp.clear();
        }
        this->ChargerMap(calque);
        multiCalques.push_back(calque);
        calque.clear();
        fichier >> param[0];
    }while(param[0] == -1 && !fichier.eof());
    fichier.close();

    return true;
}

bool Mapping::ChargerMap(std::vector <std::vector<Tile> >&calque)
{
    //Algorithme d�coupant les blocs en fonction de leurs ID (ID comprit entre 0 et (m_nbTileHauteur*m_nbTileLargeur)-1 )
    for(unsigned int i = 0 ; i < calque.size() ; i++)
    {
        for(unsigned int j = 0 ; j < calque[i].size() ; j++)
        {
            int bloc = calque[i][j].bloc; //On attribut � bloc l'ID du bloc sur lequel on va travailler (pour simplifier les caculs)
            if(bloc >= 0)
            {
                if(bloc > (m_nbTileHauteur*m_nbTileLargeur)-1) //Si l'ID n'est pas comprise dans le tileset.bmp
                    return false;

                int y =  int((bloc/m_nbTileLargeur)); //y � l'endroit (en y) ou ce trouve la case dans le tileset.bmp
                int x = (bloc-(y*m_nbTileLargeur))%m_nbTileLargeur; //idem en x

                y *= m_resolutionHauteur;
                x *= m_resolutionLargeur;

                calque[i][j].sprite.setTexture(m_refText);
                calque[i][j].sprite.setTextureRect(sf::IntRect(x,  y,  m_resolutionLargeur, m_resolutionHauteur)); //On d�coupe la case dans le tileset.bmp
                calque[i][j].sprite.setPosition(j*m_resolutionLargeur, i*m_resolutionHauteur); //On le place � la bonne position
            }
        }
    }


    return true;
}

void Mapping::Draw()
{
    //Affichage de chaque tile de chaque Calque
    for(unsigned int k = 0 ; k < multiCalques.size() ; k++)
        for(unsigned int i = 0 ; i < multiCalques[k].size() ; i++)
            for(unsigned int j = 0 ; j < multiCalques[k][i].size() ; j++)
                fen->draw(multiCalques[k][i][j].sprite);

}

bool Mapping::TestCollision(sf::Sprite *sprite, sf::Vector2f position, Direction direction)
{
    if(position.x < 0 || position.y < 0 || position.x > m_nbBlocsLargeur*m_resolutionLargeur || position.y > m_nbBlocsHauteur*m_resolutionHauteur)
        return true;

    unsigned int x;
    unsigned int y;

    if(direction == BAS)
    {
        x = (position.x / m_resolutionLargeur);
        y = (position.y / m_resolutionHauteur);
        if(y >= multiCalques[this->calqueCollision].size() || x >= multiCalques[this->calqueCollision][y].size())
            return true;
        for(unsigned int i = 0 ; i < m_vide.size() ; i++)
        {
            if(multiCalques[this->calqueCollision][y][x].bloc == m_vide[i])
                return false;
        }
    }
    else if(direction == HAUT)
    {
        x = (position.x / m_resolutionLargeur);
        y = (position.y / m_resolutionHauteur);
        if(y > multiCalques[this->calqueCollision].size() || x > multiCalques[this->calqueCollision][y].size())
            return false;
        for(unsigned int i = 0 ; i < m_vide.size() ; i++)
        {
            if(multiCalques[this->calqueCollision][y][x].bloc == m_vide[i])
                return false;
        }
    }
    else if(direction == DROITE)
    {
        x = (position.x / m_resolutionLargeur);
        y = (position.y / m_resolutionHauteur);
        if(y > multiCalques[this->calqueCollision].size() || x > multiCalques[this->calqueCollision][y].size())
            return false;
        for(unsigned int i = 0 ; i < m_vide.size() ; i++)
        {
            if(multiCalques[this->calqueCollision][y][x].bloc == m_vide[i])
                return false;
        }
    }
    else if(direction == GAUCHE)
    {
        x = (position.x / m_resolutionLargeur);
        y = (position.y / m_resolutionHauteur);
        if(y > multiCalques[this->calqueCollision].size() || x > multiCalques[this->calqueCollision][y].size())
            return false;
        for(unsigned int i = 0 ; i < m_vide.size() ; i++)
        {
            if(multiCalques[this->calqueCollision][y][x].bloc == m_vide[i])
                return false;
        }
    }

    return true;
}

sf::Vector2f Mapping::getSpawn()
{
    return spawn;
}

int Mapping::getRosolutionLargeur()
{
    return m_resolutionLargeur;
}
int Mapping::getResolutionHauteur()
{
    return m_resolutionHauteur;
}
