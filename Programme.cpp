/**
 * \file Programme.cpp
 * \brief Impl�mentaton classe principale
 * \author J.Cassagne
 */


#include "Programme.h"

Programme::Programme()
{
    Fipps.loadFromFile(POLICE+std::string("Fipps.otf"));
}

Programme::~Programme()
{
}


int Programme::Menu()
{
    musique = new Son(SON+std::string("D_I.ogg"), true);
    musique->setVolume(50);
    musique->Lire();

    sf::Texture textureMenu;
    textureMenu.loadFromFile(TEXTURE+std::string("fondMenu.png"));
    App = new sf::RenderWindow(sf::VideoMode(textureMenu.getSize().x, textureMenu.getSize().y), "Isenmon - Menu", sf::Style::Titlebar);
    App->setFramerateLimit(30);

    sf::Text item[4];
    int selection = 0;
    sf::Text selecteur;
    sf::Font Fipps;
    sf::Sprite fondMenu;

    fondMenu.setTexture(textureMenu);
    fondMenu.setPosition((App->getSize().x/2) - (textureMenu.getSize().x/2),(App->getSize().y/2) - (textureMenu.getSize().y/2));

    Fipps.loadFromFile(POLICE+std::string("Fipps.otf"));
    item[0].setString("Nouvelle partie");
    item[1].setString("Continuer");
    item[2].setString("Options");
    item[3].setString("Quitter");
    selecteur.setString("int");
    for(int i = 0 ; i < 4 ; i++)
    {
        item[i].setFont(Fipps);
        item[i].setPosition(fondMenu.getPosition().x + (textureMenu.getSize().x/4), (fondMenu.getPosition().y+100) + 75*i);
        item[i].setColor(sf::Color(0,0,0));
    }
    selecteur.setFont(Fipps);
    selecteur.setPosition(item[0].getPosition().x-75, item[0].getPosition().y);
    selecteur.setColor(sf::Color(50, 120, 250));
    selecteur.setScale(0.5, 0.5);
    bool fin = false;
    while (!fin)
    {
        sf::Event Event;
        while (App->pollEvent(Event))
        {
            if (Event.type == sf::Event::Closed)
            {
                selection = 3;
                fin = true;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && (selection < 3))
            {
                selection++;
                selecteur.move(0, 75);
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && (selection > 0))
            {
                selection--;
                selecteur.move(0, -75);
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
                fin = true;
            if (Event.type == sf::Event::MouseWheelMoved)
            {
                int pas = -Event.mouseWheel.delta;
                if((pas > 0) && (selection < 3))
                {
                    selection++;
                    selecteur.move(0, 75);
                }
                else if((pas < 0) && (selection > 0))
                {
                    selection--;
                    selecteur.move(0, -75);
                }
            }
            if(Event.type == sf::Event::MouseMoved)
            {
                int y = Event.mouseMove.y;
                y -= item[0].getPosition().y-37.5;
                if(y >0 && y < 4*75)
                {
                    if(y < 75)
                    {
                        selecteur.move(0, -selection*75);
                        selection = 0;
                    }
                    else if(y < (2*75))
                    {
                        selecteur.move(0, (1-selection)*75);
                        selection = 1;
                    }
                    else if(y < (3*75))
                    {
                        selecteur.move(0, (2-selection)*75);
                        selection = 2;
                    }
                    else if(y < (4*75))
                    {
                        selecteur.move(0, (3-selection)*75);
                        selection = 3;
                    }
                }
            }
            if (Event.type == sf::Event::MouseButtonPressed)
            {
                if (Event.mouseButton.button == sf::Mouse::Left)
                {
                    int y = Event.mouseButton.y;
                    y -= item[0].getPosition().y-37.5;
                    int x = Event.mouseButton.x;
                    if(y >= 0 && y <= 4*75)
                        if(x >= (item[0].getPosition().x-75) && x <= (item[0].getPosition().x-75+textureMenu.getSize().x))
                            fin=true;
                }
            }

        }

        App->clear(sf::Color(0, 0, 0));
        App->draw(fondMenu);
        for(int i = 0 ; i < 4 ; i++)
        {
            App->draw(item[i]);
        }
        App->draw(selecteur);
        App->display();
    }
    vue = App->getDefaultView();
    for(int i = 0 ; i < 10 ; i++)
    {
        musique->setVolume(5*(10-i));
        vue.rotate(60);
        vue.zoom(1.7);
        App->setView(vue);
        App->clear(sf::Color(0, 0, 0));
        App->draw(fondMenu);
        for(int i = 0 ; i < 4 ; i++)
        {
            App->draw(item[i]);
        }
        App->draw(selecteur);
        App->display();
    }

    App->close();
    delete App;
    musique->Stop();
    delete musique;


    return selection;
}

bool Programme::ChargementJeu()
{
    #ifdef _RELEASE
    App = new sf::RenderWindow(sf::VideoMode::getDesktopMode(), "Isenmon", sf::Style::Fullscreen);
    #elif _DEBUG
    App = new sf::RenderWindow(sf::VideoMode(1360, 600), "Isenmon", sf::Style::Titlebar);
    #endif
    App->setFramerateLimit(30);
    App->setVerticalSyncEnabled(true);
    App->setMouseCursorVisible(false);
    App->setKeyRepeatEnabled(false);
    std::cout << "test\n";


    sf::Text texte;
    texte.setFont(Fipps);
    texte.setString("Toute similitude avec des personnes ou \nfaits r�els n'est que pure co�ncidence.");
    texte.setCharacterSize(30);
    texte.setColor(sf::Color(255, 255, 255));
    texte.setPosition((App->getSize().x/2)-(texte.getGlobalBounds().width/2), (App->getSize().y/2)-(texte.getGlobalBounds().height/2));
    sf::Clock clock;
    App->clear();
    App->draw(texte);
    App->display();

    player = new joueur("guigui");

    terrain = new Mapping(App);
    terrain->LireFichier(MAPS+std::string("Etage5.txt"));

    evenements = new Evenements();
    evenements->LireFichier(MAPS+std::string("Etage5Event.txt"));

    perso = new Personnage(ANIMATION+std::string("Perso"));
    posActuel = terrain->getSpawn();

    vue = App->getDefaultView();
    vue.zoom(ZOOM);
    vue.setCenter(posActuel.x+(perso->getSize().x/2), posActuel.y+(perso->getSize().y/2));

    textObjectif.loadFromFile(TEXTURE + std::string("objectif.png"));
    sprObjectif.setTexture(textObjectif);
    sprObjectif.setPosition(vue.getCenter());
    sprObjectif.setColor(sf::Color(150, 150, 150, 50));
    toDrawJeu.push_back(&sprObjectif);

    musique = new Son(SON+std::string("fond.ogg"), true);
    cle = new Son(SON+std::string("objectif.ogg"), false);

    #ifdef _RELEASE
    bool fin = false;
    while(!fin)
    {
        float opacite = 255, temps = clock.getElapsedTime().asSeconds();
        if(temps >= 3)
            opacite -= 255*(temps-3);
        if(temps > 4)
            fin = true;
        opacite = (opacite<0) ? 0 : opacite;
        texte.setColor(sf::Color(255, 255, 255, opacite));
        App->clear();
        App->draw(texte);
        App->display();

    }
    #endif // _RELEASE
    App->setView(vue);
    musique->Lire();

    return true;
}
bool Programme::DechargementJeu()
{
    delete App;
    delete terrain;
    delete evenements;
    delete perso;
    musique->Stop();
    delete musique;
    delete cle;
    delete player;
    return true;
}

int Programme::Jeu(bool nouvellePartie)
{
    ChargementJeu();

    if(nouvellePartie)
    {
        App->clear();
        this->DrawJeu();
        App->display();
        for(unsigned int i = 0 ; i < evenements->Intro().size() ; i++)
            Notification(evenements->Intro()[i]);
    }

    std::string notif = "";

    while (App->isOpen())
    {
        sf::Event Event;
        while (App->pollEvent(Event))
        {
            if (Event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
                App->close();


            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
                etat = GAUCHE;
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
                etat = DROITE;
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
                etat = BAS;
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
                etat = HAUT;
            if(!sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && !sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && !sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && !sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
                etat = ATTEND;
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::P))
                Notification("Vous �tes en :\n" + itos((int)(posActuel.x/terrain->getRosolutionLargeur())) + ", " + itos((int)(posActuel.y/terrain->getRosolutionLargeur())));
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::C))
            {
                musique->Pause();
                if(Combat() == 0)
                    Notification("Bravo !\nL'isenmon est d�c�d� dans d'atroces\nsouffrances !");
                else
                    posActuel = terrain->getSpawn();
                musique->Lire();
            }
        }

        switch(etat)
        {
        case GAUCHE:
            perso->Avancer(GAUCHE);
            if(!terrain->TestCollision(perso->getSprite(), sf::Vector2f(posActuel.x-10, posActuel.y), GAUCHE))
                posActuel.x -= 5;
            break;
        case DROITE:
            perso->Avancer(DROITE);
            if(!terrain->TestCollision(perso->getSprite(), sf::Vector2f(posActuel.x+10, posActuel.y), DROITE))
                posActuel.x += 5;
            break;
        case BAS:
            perso->Avancer(BAS);
            if(!terrain->TestCollision(perso->getSprite(), sf::Vector2f(posActuel.x, posActuel.y+5), BAS))
                posActuel.y += 5;
            break;
        case HAUT:
            perso->Avancer(HAUT);
            if(!terrain->TestCollision(perso->getSprite(), sf::Vector2f(posActuel.x, posActuel.y-5), HAUT))
                posActuel.y -= 5;
            break;
        case ATTEND:
            perso->Stop();
        }

        if((sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) && (clock.getElapsedTime().asMilliseconds() > 250))
        {
            notif = evenements->Message((int)(posActuel.x/terrain->getRosolutionLargeur()), (int)(posActuel.y/terrain->getResolutionHauteur()));
            if(notif != "")
            {
                perso->Stop();
                if(evenements->TestObjectif((int)(posActuel.x/terrain->getRosolutionLargeur()), (int)(posActuel.y/terrain->getResolutionHauteur())))
                {
                    sprObjectif.setColor(sf::Color(255, 255, 255, 255));
                    cle->Lire();
                }
                Notification(notif);
            }
            clock.restart();
        }

        this->DrawJeu();
        App->display();
    }

    DechargementJeu();
    return 0;
}

void Programme::DrawJeu()
{
    perso->setPosition(posActuel);
    vue.setCenter(posActuel.x+(perso->getSize().x/2), posActuel.y+(perso->getSize().y/2));
    sprObjectif.setPosition(vue.getCenter().x-(ZOOM*(App->getSize().x/2)), vue.getCenter().y-(ZOOM*(App->getSize().y/2)));
    App->setView(vue);

    App->clear();
    terrain->Draw();
    for(unsigned int i = 0 ; i < toDrawJeu.size() ; i++)
        App->draw(*toDrawJeu[i]);

    App->draw(*perso->getSprite());
}


void Programme::Notification(std::string str)
{
    sf::Texture textureCadre;
    sf::Sprite cadre;
    sf::Text texte;
    textureCadre.loadFromFile(TEXTURE+std::string("cadre.png"));
    cadre.setTexture(textureCadre);
    cadre.setPosition(posActuel.x - (App->getSize().x/3), posActuel.y + (App->getSize().x/8));

    texte.setString(str);
    texte.setFont(Fipps);
    texte.setPosition(cadre.getPosition().x +20 , cadre.getPosition().y +20 );
    texte.setColor(sf::Color(0, 0, 0));
    texte.setScale(0.5, 0.5);

    bool lu = false;
    while (!lu)
    {
        sf::Event Event;
        while(App->pollEvent(Event))
        {
            if (Event.type == sf::Event::KeyPressed || Event.type == sf::Event::MouseButtonPressed)
                if ((Event.key.code == sf::Keyboard::Return) || (Event.mouseButton.button == sf::Mouse::Left))
                    lu = true;
        }
        this->DrawJeu();
        App->draw(cadre);
        App->draw(texte);
        App->display();
    }
}

void Programme::AttendreEntree()
{
    bool fin = false;
    sf::Event Event;
    clock.restart();
    while(!fin)
    {
        App->waitEvent(Event);
        if(clock.getElapsedTime().asMilliseconds() > 50)
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
                fin = true;
    }
}


int Programme::Combat()
{
    /// TODO (Julien#7#): Impl�menter l'option "changer de pokemon" et ensuite le sac

    Son musCombat(SON+std::string("battle.ogg"), true);
    musCombat.setVolume(25);
    isen = new isenmon(1);
    isen->affiche();
    bool fin = false;
    bool menuAttaque = false;
    int selection = 0;
    App->setView(App->getDefaultView());
    sf::Texture papier;
    papier.loadFromFile(TEXTURE+std::string("papier.jpg"));
    papier.setRepeated(true);

    sf::Texture textCercle[2];
    textCercle[0].loadFromFile(TEXTURE+std::string("plateformeHaut.png"));
    textCercle[1].loadFromFile(TEXTURE+std::string("plateformeBas.png"));
    sf::Sprite cercle[2];
    cercle[0].setTexture(textCercle[0]);
    cercle[1].setTexture(textCercle[1]);
    toDrawCombat.push_back(&cercle[0]);
    toDrawCombat.push_back(&cercle[1]);

    sf::RectangleShape rectChoix(sf::Vector2f(App->getSize().x/3, App->getSize().y/5));
    rectChoix.setPosition(App->getSize().x-(App->getSize().x/3)-5, App->getSize().y-(App->getSize().y/5)-5);
    rectChoix.setOutlineThickness(1);
    rectChoix.setOutlineColor(sf::Color(175, 175, 175));
    rectChoix.setTexture(&papier);
    toDrawCombat.push_back(&rectChoix);

    sf::RectangleShape rectCommentaire(sf::Vector2f(2*(App->getSize().x/3), App->getSize().y/5));
    rectCommentaire.setPosition(5, App->getSize().y-(App->getSize().y/5)-5);
    rectCommentaire.setOutlineThickness(1);
    rectCommentaire.setOutlineColor(sf::Color(175, 175, 175));
    rectCommentaire.setTexture(&papier);
    toDrawCombat.push_back(&rectCommentaire);

    sf::Image imgFondCadre[2];
    imgFondCadre[0].loadFromFile(TEXTURE+std::string("fondCadre.png"));
    imgFondCadre[0].createMaskFromColor(sf::Color::White);
    imgFondCadre[1].loadFromFile(TEXTURE+std::string("fondCadre.png"));
    imgFondCadre[1].createMaskFromColor(sf::Color::White);
    imgFondCadre[1].flipHorizontally();
    imgFondCadre[1].flipVertically();
    sf::Texture fondCadre[2];
    fondCadre[0].loadFromImage(imgFondCadre[0]);
    fondCadre[1].loadFromImage(imgFondCadre[1]);

    sf::Sprite cadre[2];
    cadre[0].setTexture(fondCadre[0]);
    cadre[1].setTexture(fondCadre[1]);
    cadre[0].setPosition(0,0);
    cadre[1].setPosition(App->getSize().x-cadre[1].getGlobalBounds().width,(App->getSize().y-(App->getSize().y/5)-5)-cadre[1].getGlobalBounds().height);
    toDrawCombat.push_back(&cadre[0]);
    toDrawCombat.push_back(&cadre[1]);

    sf::Text choix[4], pv[2], commentaire, selecteur, nom[2];
    choix[0].setString("Attaquer");
    choix[0].setPosition(App->getSize().x-(App->getSize().x/3)+50, App->getSize().y-(App->getSize().y/5)+10);
    choix[1].setString("Isenmon");
    choix[1].setPosition(App->getSize().x-(App->getSize().x/3)+50, App->getSize().y-(App->getSize().y/5)+(rectChoix.getSize().y/1.5));
    choix[2].setString("Sac");
    choix[2].setPosition(App->getSize().x-(App->getSize().x/3)+(rectChoix.getSize().x/1.5), App->getSize().y-(App->getSize().y/5)+10);
    choix[3].setString("Fuyons !");
    choix[3].setPosition(App->getSize().x-(App->getSize().x/3)+(rectChoix.getSize().x/1.5), App->getSize().y-(App->getSize().y/5)+(rectChoix.getSize().y/1.5));


    commentaire.setString("\nQue voulez-vous faire :");
    commentaire.setPosition(10, App->getSize().y-(App->getSize().y/5)+10);
    commentaire.setCharacterSize(20);
    commentaire.setFont(Fipps);
    commentaire.setColor(sf::Color::Black);
    toDrawCombat.push_back(&commentaire);

    selecteur.setString("int");
    selecteur.setFont(Fipps);
    selecteur.setPosition(choix[0].getPosition().x-40, choix[0].getPosition().y);
    selecteur.setColor(sf::Color(50, 120, 250));
    selecteur.setScale(0.5, 0.5);
    toDrawCombat.push_back(&selecteur);

    for(int i = 0 ; i < 4 ; i++)
    {
        choix[i].setFont(Fipps);
        choix[i].setColor(sf::Color::Black);
        choix[i].setCharacterSize(20);
    }
    for(int i = 0 ; i < 2 ; i++)
    {
        pv[i].setCharacterSize(15);
        pv[i].setFont(Fipps);
        pv[i].setColor(sf::Color::Black);
    }
    pv[1].setString(itos(player->getIsenmonActuel()->getVieActuel()) + " / " + itos(player->getIsenmonActuel()->getVie()));
    pv[0].setString(itos(isen->getVieActuel()) + " / " + itos(isen->getVie()));
    pv[0].setPosition(5, 20);
    pv[1].setPosition(App->getSize().x-pv[1].getGlobalBounds().width-10 ,  App->getSize().y-(App->getSize().y/5)-15-pv[1].getGlobalBounds().height);
    toDrawCombat.push_back(&pv[0]);
    toDrawCombat.push_back(&pv[1]);
    toDrawCombat.push_back(&choix[0]);
    toDrawCombat.push_back(&choix[1]);
    toDrawCombat.push_back(&choix[2]);
    toDrawCombat.push_back(&choix[3]);

    for(int i = 0 ; i < 2 ; i++)
    {
        nom[i].setCharacterSize(20);
        nom[i].setFont(Fipps);
        nom[i].setColor(sf::Color::Black);
    }
    nom[0].setString(isen->getNom());
    nom[1].setString(player->getIsenmonActuel()->getNom());
    nom[0].setPosition(pv[0].getPosition().x, pv[0].getPosition().y+pv[0].getGlobalBounds().height+nom[0].getGlobalBounds().height);
    nom[1].setPosition(App->getSize().x-nom[1].getGlobalBounds().width ,  pv[1].getPosition().y-(2*nom[1].getGlobalBounds().height));
    toDrawCombat.push_back(&nom[0]);
    toDrawCombat.push_back(&nom[1]);

    sf::RectangleShape rectBarreVie[2];
    for(int i = 0 ; i < 2 ; i++)
    {
        rectBarreVie[i] = sf::RectangleShape(sf::Vector2f(200, 10));
        rectBarreVie[i].setOutlineThickness(1);
        rectBarreVie[i].setOutlineColor(sf::Color::Black);
        rectBarreVie[i].setFillColor(sf::Color(230, 230, 230));
    }
    rectBarreVie[0].setPosition(1, 1);
    rectBarreVie[1].setPosition(App->getSize().x-200, App->getSize().y-(App->getSize().y/5)-20);
    toDrawCombat.push_back(&rectBarreVie[0]);
    toDrawCombat.push_back(&rectBarreVie[1]);

    sf::VertexArray barreVie[2];
    barreVie[0].resize(4);
    barreVie[0].setPrimitiveType(sf::Quads);
    barreVie[0][0] = sf::Vertex(sf::Vector2f(1,1), sf::Color(0,150, 0));
    barreVie[0][1] = sf::Vertex(sf::Vector2f(201,1), sf::Color::Green);
    barreVie[0][2] = sf::Vertex(sf::Vector2f(201,11), sf::Color::Green);
    barreVie[0][3] = sf::Vertex(sf::Vector2f(1,11), sf::Color(0,150,0));
    barreVie[1].resize(4);
    barreVie[1].setPrimitiveType(sf::Quads);
    barreVie[1][0] = sf::Vertex(sf::Vector2f(App->getSize().x,App->getSize().y-(App->getSize().y/5)-10 ), sf::Color(0,150,0));
    barreVie[1][1] = sf::Vertex(sf::Vector2f(App->getSize().x-200,App->getSize().y-(App->getSize().y/5)-10), sf::Color::Green);
    barreVie[1][2] = sf::Vertex(sf::Vector2f(App->getSize().x-200,App->getSize().y-(App->getSize().y/5)-20), sf::Color::Green);
    barreVie[1][3] = sf::Vertex(sf::Vector2f(App->getSize().x,App->getSize().y-(App->getSize().y/5)-20), sf::Color(0,150,0));
    toDrawCombat.push_back(&barreVie[0]);
    toDrawCombat.push_back(&barreVie[1]);

    BarreVie((float)isen->getVieActuel()/(float)isen->getVie(), barreVie[0]);
    BarreVie((float)player->getIsenmonActuel()->getVieActuel()/(float)player->getIsenmonActuel()->getVie(), barreVie[1]);

    player->getIsenmonActuel()->getAnim()->getSprite()->setPosition(0, (App->getSize().y-(App->getSize().y/5)-5)- player->getIsenmonActuel()->getAnim()->getSprite()->getGlobalBounds().height);
    isen->getAnim()->getSprite()->setPosition(App->getSize().x - isen->getAnim()->getSprite()->getGlobalBounds().width, 0);
    for(int i = 0 ; i < 4 ; i++)
    {
        player->getIsenmonActuel()->getActionPossible()[i].anim->getSprite()->setPosition(isen->getAnim()->getSprite()->getPosition());
        player->getIsenmonActuel()->getActionPossible()[i].anim->getSprite()->move(30, 40);
        isen->getActionPossible()[i].anim->getSprite()->setPosition(player->getIsenmonActuel()->getAnim()->getSprite()->getPosition());
        isen->getActionPossible()[i].anim->getSprite()->move(30,40);
    }
    cercle[0].setPosition(App->getSize().x-cercle[0].getGlobalBounds().width+(cercle[0].getGlobalBounds().width/4), isen->getAnim()->getSprite()->getGlobalBounds().height-((isen->getAnim()->getSprite()->getGlobalBounds().height)/3));
    cercle[1].setPosition(0, (App->getSize().y-(App->getSize().y/5)-5)- cercle[1].getGlobalBounds().height);

    sf::Event Event;
    musCombat.Lire();

    sf::Thread affichage(&Programme::DrawCombat, this);
    App->setActive(false);
    affichage.launch();
    clock.restart();

    while (!fin)
    {
        App->waitEvent(Event);
        if(clock.getElapsedTime().asMilliseconds() > 50)
        {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
            {
                mutex.lock();
                if(menuAttaque)
                {
                    menuAttaque = false;
                    choix[0].setString("Attaquer");
                    choix[1].setString("Isenmon");
                    choix[2].setString("Sac");
                    choix[3].setString("Fuyons !");
                    commentaire.setString("\nQue voulez-vous faire :");
                }
                mutex.unlock();
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
            {
                if(!menuAttaque)
                {
                    mutex.lock();
                    switch(selection)
                    {
                    case 0:
                        menuAttaque = true;
                        for(int i = 0 ; i < 4 ; i++)
                            choix[i].setString(player->getIsenmonActuel()->getActionPossible()[i].nom);
                        commentaire.setString("\nChoisissez une attaque :");
                        break;
                    case 1:
                        commentaire.setString("\nFonction indisponible.");
                        break;
                    case 2:
                        commentaire.setString("\nFonction indisponible.");
                        break;
                    case 3:
                        commentaire.setString("\nFonction indisponible.");
                        break;
                    }
                    mutex.unlock();
                }
                else
                {
                    mutex.lock();
                    choix[0].setString("");
                    choix[1].setString("");
                    choix[2].setString("");
                    choix[3].setString("");
                    selecteur.setString("");

                    player->getIsenmonActuel()->attaque(selection,*isen);
                    player->getIsenmonActuel()->getActionPossible()[selection].musiqueAnim->Lire();
                    pv[0].setString(itos(isen->getVieActuel()) + " / " + itos(isen->getVie()));
                    BarreVie((float)isen->getVieActuel()/(float)isen->getVie(), barreVie[0]);
                    commentaire.setString(player->getIsenmonActuel()->getNom() + " lance " + player->getIsenmonActuel()->getActionPossible()[selection].nom + "\n\n" + player->getIsenmonActuel()->getActionPossible()[selection].description);
                    mutex.unlock();
                    Clignoter(isen->getAnim()->getSprite(), 3);
                    AttendreEntree();
                    if(isen->getEnVie())
                    {
                        mutex.lock();
                        int attaque = NbAleatoire(0, 4);
                        isen->attaque(attaque, *player->getIsenmonActuel());
                        isen->getActionPossible()[attaque].musiqueAnim->Lire();
                        pv[1].setString(itos(player->getIsenmonActuel()->getVieActuel()) + " / " + itos(player->getIsenmonActuel()->getVie()));
                        BarreVie((float)player->getIsenmonActuel()->getVieActuel()/(float)player->getIsenmonActuel()->getVie(), barreVie[1]);
                        commentaire.setString(isen->getNom() + " ennemie lance " + isen->getActionPossible()[attaque].nom + "\n\n" + isen->getActionPossible()[attaque].description);
                        mutex.unlock();
                        Clignoter(player->getIsenmonActuel()->getAnim()->getSprite(), 3);
                        AttendreEntree();
                    }
                    selection = 0;
                    mutex.lock();
                    selecteur.setPosition(choix[0].getPosition().x-40, choix[0].getPosition().y);

                    menuAttaque = false;
                    commentaire.setString("\nQue voulez-vous faire :");
                    choix[0].setString("Attaquer");
                    choix[1].setString("Isenmon");
                    choix[2].setString("Sac");
                    choix[3].setString("Fuyons !");
                    selecteur.setString("int");
                    mutex.unlock();
                }
            }
            mutex.lock();
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && (selection==0 || selection==1))
            {
                selection+=2;
                selecteur.move(choix[2].getPosition().x-choix[0].getPosition().x, 0);
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && (selection==2 || selection==3))
            {
                selection-=2;
                selecteur.move(-(choix[2].getPosition().x-choix[0].getPosition().x), 0);
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && (selection==0 || selection==2))
            {
                selection++;
                selecteur.move(0, choix[1].getPosition().y-choix[0].getPosition().y);
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && (selection==1 || selection==3))
            {
                selection--;
                selecteur.move(0, -(choix[1].getPosition().y-choix[0].getPosition().y));
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
                fin = true;
            if(!player->getIsenmonActuel()->getEnVie())
            {
                commentaire.setString("\nVous etes DCD");
                fin = true;
            }
            if(!isen->getEnVie())
            {
                commentaire.setString("\nL'isenmon en face de vous est DCD");
                fin = true;
            }

            if(fin)
            {
                choix[0].setString("");
                choix[1].setString("");
                choix[2].setString("");
                choix[3].setString("");
                selecteur.setString("");
            }
            mutex.unlock();
            clock.restart();
        }
    }
    AttendreEntree();
    musCombat.Stop();
    mutex.lock();
    toDrawCombat.clear();
    mutex.unlock();
    affichage.wait();
    App->setActive(true);
    delete isen;
    App->setView(vue);
    return (isen->getEnVie()) ? 1 : 0;
}

void Programme::DrawCombat()
{
    App->setActive(true);
    sf::Clock clockAnim[2];
    int nbAlea[2] = {NbAleatoire(0, 7), NbAleatoire(0, 7)};
    while(toDrawCombat.size() >= 1)
    {
        if(clockAnim[0].getElapsedTime().asSeconds() > nbAlea[0])
        {
            player->getIsenmonActuel()->getAnim()->Relancer();
            nbAlea[0] = NbAleatoire(1, 7);
            clockAnim[0].restart();
        }
        if(clockAnim[1].getElapsedTime().asSeconds() > nbAlea[1])
        {
            isen->getAnim()->Relancer();
            nbAlea[1] = NbAleatoire(1, 7);
            clockAnim[1].restart();
        }
        App->clear(sf::Color(255, 255, 255));
        mutex.lock();
        for(unsigned int i = 0 ; i < toDrawCombat.size() ; i++)
            App->draw(*toDrawCombat[i]);
        mutex.unlock();
        App->draw(*player->getIsenmonActuel()->getAnim()->getSprite());
        App->draw(*isen->getAnim()->getSprite());
        for(int i = 0 ; i < 4 ; i++)
        {
            if(!player->getIsenmonActuel()->getActionPossible()[i].anim->FinAnimation())
                App->draw(*player->getIsenmonActuel()->getActionPossible()[i].anim->getSprite());
            if(!isen->getActionPossible()[i].anim->FinAnimation())
                App->draw(*isen->getActionPossible()[i].anim->getSprite());
        }
        App->display();
    }
    App->setActive(false);
}

void Programme::BarreVie(float rapport, sf::VertexArray &barre)
{
    sf::Color couleur[2];
    float RGB[3];
    RGB[0] = (((1-rapport)*255)-100);
    RGB[1] = ((rapport*255)-50);
    RGB[2] = 0;
    if(RGB[0] < 0)
        RGB[0] = 0;
    if(RGB[1] < 0)
        RGB[1] = 0;
    couleur[0].r = RGB[0];
    couleur[0].g = RGB[1];
    couleur[0].b = RGB[2];
    RGB[0] = ((1-rapport)*255);
    RGB[1] = (rapport*255);
    couleur[1].r = RGB[0];
    couleur[1].g = RGB[1];
    couleur[1].b = RGB[2];

    barre[0] = sf::Vertex(sf::Vector2f(barre[0].position.x, barre[0].position.y), couleur[0]);
    barre[3] = sf::Vertex(sf::Vector2f(barre[3].position.x, barre[3].position.y), couleur[0]);
    barre[2] = sf::Vertex(sf::Vector2f(barre[2].position.x, barre[2].position.y), couleur[1]);
    barre[1] = sf::Vertex(sf::Vector2f(barre[1].position.x, barre[1].position.y), couleur[1]);
    if(barre[0].position.y == 1)
    {
        barre[1] = sf::Vertex(sf::Vector2f(barre[1].position.x-(barre[1].position.x - (rapport*200)),barre[1].position.y), barre[1].color);
        barre[2] = sf::Vertex(sf::Vector2f(barre[2].position.x-(barre[2].position.x - (rapport*200)),barre[2].position.y), barre[2].color);
    }
    else
    {
        barre[1] = sf::Vertex(sf::Vector2f(barre[0].position.x-(rapport*200),barre[1].position.y), barre[1].color);
        barre[2] = sf::Vertex(sf::Vector2f(barre[3].position.x-(rapport*200),barre[2].position.y), barre[2].color);
    }
}

void Programme::Clignoter(sf::Sprite *spr, unsigned int clignotement)
{
    sf::Vector2f position = spr->getPosition();
    for(unsigned int i = 0; i < clignotement ; i++)
    {
        #ifdef __linux__
        usleep(200);
        #else
        Sleep(200);
        #endif
        spr->setPosition(0, -spr->getGlobalBounds().height);
        #ifdef __linux__
        usleep(200);
        #else
        Sleep(100);
        #endif
        spr->setPosition(position);
    }

}





int Programme::NbAleatoire(int a, int b)
{
    return rand()%(b-a) +a;
}
