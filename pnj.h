/**
 * \file pnj.h
 * \brief Gestion des PNJ
 * \author G.Fitas
 */


#ifndef PNJ_H
#define PNJ_H
#include "isenmon.h"
#include "Personnage.h"
#define DATAPNJ "data/ressources/pnj/"

class pnj
{
    public:
        pnj(std::string pnj);
        ~pnj();

        std::string getNom();
        std::string getNdf();
        isenmon* getIsenmonActuel();
        std::vector<isenmon*> getListeIsenmon();
        int getNbIsenmon();

        void changerIsenmon(int);
        void aller(float x,float y);

    private:

        std::string m_nom;
        std::string m_ndf;
        int m_pos_x;
        int m_pos_y;
        int m_porte;
        Personnage *m_perso;
        isenmon * m_isenmonActuel;
        int m_nbisenmon;
        std::vector<isenmon*> m_listeIsenmon;
};

#endif // PNJ_H
