/**
 * \file Animation.h
 * \brief Gestion des animations
 * \author J.Cassagne
 */

#ifndef ANIMATION_H
#define ANIMATION_H

#define ANIMATION "./data/anim/"

#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>
#include <string>
#include <fstream>

class Animation
{
    public:
        Animation(std::string nomFichier);
        sf::Sprite* getSprite();
        bool FinAnimation();
        sf::Vector2u getSize();
        void Relancer();
        void setImageActuelle(int img);
        void Stop();

    private:
        void FinTimer();
        bool LireFichier(std::string nomFichier);

        sf::Texture image;
        sf::Sprite spr;
        std::vector<sf::Sprite> tableauImages;
        sf::Clock* clock;
        int nbImages, chiffreImageActuel, largeur, hauteur, espace;
        float nbSecondes;
        bool repetition, fin;

};

#endif // ANIMATION_H
