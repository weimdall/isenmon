/**
 * \file isenmon.cpp
 * \brief Gestion des isenmons
 * \author G.Fitas
 */

#include "isenmon.h"

using namespace std;

int stoi(string in)
{
    int numb;
    istringstream ( in ) >> numb;
    return numb;
}

string itos(int in)
{
    std::string s;
    std::stringstream out;
    out << in;
    s = out.str();
    return s;
}
void isenmon::cpy(isenmon in)
{
    m_id = in.getId();
    m_nom = in.getNom();
    m_type = in.getType();
    m_vie = in.getVie();
    m_vieActuel = in.getVieActuel();
    m_etat = in.getEtat();
    m_etatTourRestant = in.getEtatTourRestant();
    m_level = in.getLevel();
    m_ndf = in.getNdf();
    m_actionPossible = in.getActionPossible();
    m_stringNomActionPossible = in.getStringNomActionPossible();
    m_possedeur = in.getPossedeur();
    m_nombreAttaquePossible = in.getNombreAttaquePossible();
    m_anim = in.getAnim();
    m_musiqueAnim = in.getMusiqueAnim();
}
isenmon::isenmon(int id,std::string nom)
{
    if(id>0)
    {
        m_possedeur = nom;
        string liste = datasave + nom + "/listIsenmon";
        ifstream fileListe(liste.c_str(),ios::in);
        string temp;
        if(fileListe)
        {
            for(int i=0;i<=id;i++)
            {
                getline(fileListe,temp);
            }
            if(temp!="")
            {
                m_ndf=datasave + nom + "/" + temp;
                fileListe.close();
                ifstream isen(m_ndf.c_str(),ios::in);
                if(isen)    //lecture du fichier isenmon
                {
                    getline(isen,temp); // ID
                    m_id = stoi(temp.c_str());
                    getline(isen,temp); // nom
                    m_nom = temp;
                    getline(isen,temp); // type
                    m_type = stoi(temp.c_str());
                    getline(isen,temp); // vie
                    m_vie = stoi(temp.c_str());
                    getline(isen,temp); // vie actuel
                    m_vieActuel = stoi(temp.c_str());
                    getline(isen,temp); // etat
                    m_etat = stoi(temp.c_str());
                    getline(isen,temp); // tour restant etat
                    m_etatTourRestant = stoi(temp.c_str());
                    getline(isen,temp); // niveau
                    m_level = stoi(temp.c_str());
                    getline(isen,temp); // animation de l'isenmon
                    if(nom != "terrain")
                        temp.insert(temp.end()-4, 'D');
                    m_anim = new Animation(ANIMATION + temp);
                    getline(isen,temp); // son de l'isenmon
                    m_musiqueAnim = new Son(SON + temp,false);
                    for(int i=0;i<4;i++) // on remplit le tableau d'acion possible de l'isenmon
                    {
                        getline(isen,temp); // on recupere le nom de l'action qui est aussi le nom du fichier situer dans data
                        m_stringNomActionPossible.push_back(temp);

                        string nbrestantaction;// on recupere les nombre de fois que l'attaque peut etre utilis�
                        getline(isen,nbrestantaction);
                        m_nombreAttaquePossible.push_back(stoi(nbrestantaction.c_str()));
                        cout<<"Attaque "<<i+1<<" reste "<<m_nombreAttaquePossible[i]<<" fois"<<endl; // la ca marche


                        temp= data +temp;
                        cout<<"L'action selection� se situe ici : "<<temp<<endl;
                        ifstream actionIsen(temp.c_str(),ios::in);
                        if(actionIsen)
                        {
                            action act;
                            getline(actionIsen,temp); // nom de l'action
                            act.nom = temp;
                            getline(actionIsen,temp); // attaque de l'action
                            act.attaque = stoi(temp.c_str());
                            getline(actionIsen,temp); // soins de l'action
                            act.soins = stoi(temp.c_str());
                            getline(actionIsen,temp); // description de l'action
                            act.description = temp;
                            getline(actionIsen,temp); // type de l'action
                            act.type = stoi(temp.c_str());
                            getline(actionIsen,temp); // etat de l'attaquant de l'action
                            act.etatAttaquant = stoi(temp.c_str());
                            getline(actionIsen,temp); // etat de l'attaque de l'action
                            act.etatAttaque = stoi(temp.c_str());
                            getline(actionIsen,temp); // nombre d'attaque possible au maximum
                            act.nombreAttaquePossibleMax = stoi(temp.c_str());
                            getline(actionIsen,temp); // animation
                            act.anim = new Animation(ANIMATION + temp);
                            act.anim->Stop();
                            getline(actionIsen,temp); // musique anim
                            act.musiqueAnim = new Son(SON + temp,false);
                            m_actionPossible.push_back(act);
                        }
                        else
                        {
                            cout<<"L'action selectione n'existe pas"<<endl;
                        }
                    }
                }
                else
                {
                    cout<<"L'isenmon " << temp << " n'existe pas"<<endl;
                    getchar();
                }
            }
        }
        else
        {
            cout<<"Le fichier listIsenmon est introuvable"<<endl;
            getchar();
        }
    }
    else
    {
        cout<<"L'ID entre est pourris"<<endl;
    }
    for(int i=0;i<4;i++)
    {
        cout<<m_nombreAttaquePossible[i]<<endl<<m_stringNomActionPossible[i]<<endl;
    }
}
isenmon::isenmon(int id)
{
    isenmon temp(id,"terrain");
    cpy(temp);
}

std::string isenmon::getNom()
{
    return m_nom;
}
std::string isenmon::getNdf()
{
    return m_ndf;
}
bool isenmon::getEnVie()
{
    if(m_vieActuel<=0)
        return false;
    else
        return true;
}
Son *isenmon::getMusiqueAnim()
{
    return m_musiqueAnim;
}
Animation *isenmon::getAnim()
{
    return m_anim;
}
int isenmon::getId()
{
    return m_id;
}
string isenmon::getPossedeur()
{
    return m_possedeur;
}
int isenmon::getType()
{
    return m_type;
}
int isenmon::getVieActuel()
{
    return m_vieActuel;
}
int isenmon::getVie()
{
    return m_vie;
}
int isenmon::getLevel()
{
    return m_level;
}
int isenmon::getEtat()
{
    return m_etat;
}
int isenmon::getEtatTourRestant()
{
    return m_etatTourRestant;
}
vector<string> isenmon::getStringNomActionPossible()
{
    return m_stringNomActionPossible;
}
vector<action> isenmon::getActionPossible()
{
    return m_actionPossible;
}
vector<int> isenmon::getNombreAttaquePossible()
{
    return m_nombreAttaquePossible;
}

void isenmon::setType(int t)
{
    m_type = t;
}
void isenmon::setVieActuel(int vieactuel)
{
    m_vieActuel = vieactuel;
}
void isenmon::setVie(int vie)
{
    m_vie = vie;
}
void isenmon::setLevel(int lvl)
{
    m_level = lvl;
}
void isenmon::setEtat(int etat)
{
    m_etat = etat;
}
void isenmon::setEtatTourRestant(int trrest)
{
    m_etatTourRestant = trrest;
}
void isenmon::setNombreAttaquePossible(int nmAct,int nb)
{
    if(nb>=m_actionPossible[nmAct].nombreAttaquePossibleMax)
        m_nombreAttaquePossible[nmAct] = m_actionPossible[nmAct].nombreAttaquePossibleMax;
    else
        m_nombreAttaquePossible[nmAct] = nb;
}

void isenmon::attaque(int idact,isenmon& isen) // compris entre 1 et 4 compris
{

    double pv = getActionPossible()[idact].attaque;
    double soins = getActionPossible()[idact].soins;
    int typeAttaquant = getActionPossible()[idact].type;
    int typeAttaque = isen.getType();

    if(m_etat==1) // etat paralyse
        pv=0;
    if(m_etat==2) // etat confus
    {
        if(rand()%(100)<60)
            pv=0;
        if((rand()%(100)<50)&&(getActionPossible()[idact].soins==0))
            soins = -m_vieActuel/10;
    }
    if(m_etat==3) // etat endormis
        pv = 0;
    if(m_etat==4) // etat psn
    {
        soins = -m_vieActuel/10;
    }



    if((typeAttaquant==2)&&(typeAttaque==3))
       pv=pv*1.2;
    if((typeAttaquant==4)&&((typeAttaque==2)||(typeAttaque==6)))
       pv=pv*1.1;
    if(((typeAttaquant==5)||(typeAttaquant==4))&&(typeAttaque==3))
       pv=pv*1.15;
    if(((typeAttaquant==2)||(typeAttaquant==6))&&(typeAttaque==5))
       pv=pv*1.05;

    if((typeAttaquant==3)&&(typeAttaque==2))
       pv=pv/1.2;
    if(((typeAttaquant==2)||(typeAttaquant==6))&&(typeAttaque==3))
       pv=pv/1.1;
    if(((typeAttaque==5)||(typeAttaque==4))&&(typeAttaquant==3))
       pv=pv/1.15;
    if(((typeAttaque==2)||(typeAttaque==6))&&(typeAttaquant==5))
       pv=pv/1.05;

    isen.enleverPv(pv);


    ajouterPv(soins);
    setEtat(getActionPossible()[idact].etatAttaquant);
    setEtatTourRestant(sqrt(m_level));
    isen.setEtat(getActionPossible()[idact].etatAttaque);
    isen.setEtatTourRestant(sqrt(m_level));

    getActionPossible()[idact].anim->Relancer();

}
void isenmon::tourSuivant()
{
    if(m_etatTourRestant>0)
        m_etatTourRestant--;
    if(m_etatTourRestant==0)
        m_etat=0;
}
void isenmon::enleverPv(int pv)
{
    if(m_vieActuel<pv)
    {
        m_vieActuel = 0;
    }
    else
    {
        m_vieActuel = m_vieActuel - pv;
    }
}
void isenmon::ajouterPv(int pv)
{
    if(m_vieActuel+pv<m_vie)
    {
        m_vieActuel += pv;
    }
    else
    {
        m_vieActuel = m_vie;
    }
}
void isenmon::affiche()
{
    cout<<"Votre isenmon est : "<<getNom()<<endl;
    cout<<"Vie actuel : "<<getVieActuel()<<endl;
    cout<<"Vie : "<<getVie()<<endl;
    cout<<"Type : "<<getType()<<endl;
    cout<<"Niveau : "<<getLevel()<<endl;
    cout<<"Etat : "<<getEtat()<<" pendant : "<<getEtatTourRestant()<<" tour(s)"<<endl;
    cout<<"Votre isenmon peut : "<<endl;
    cout<<"1 "<<getStringNomActionPossible()[0]<<endl;
    cout<<"2 "<<getStringNomActionPossible()[1]<<endl;
    cout<<"3 "<<getStringNomActionPossible()[2]<<endl;
    cout<<"4 "<<getStringNomActionPossible()[3]<<endl;
    for(int i=0;i<4;i++)
    {
        cout<<endl<<m_nombreAttaquePossible[i]<<endl<<m_stringNomActionPossible[i];
    }
}
void isenmon::setNdf(string ndf)
{
    m_ndf = ndf;
}
void isenmon::setPossedeur(string nom)
{
    m_possedeur = nom;
}
void isenmon::sauver()
{
    ofstream sauveIsen(m_ndf.c_str(),ios::out|ios::trunc);
    sauveIsen<<m_id<<endl;
    sauveIsen<<m_nom<<endl;
    sauveIsen<<m_type<<endl;
    sauveIsen<<m_vie<<endl;
    sauveIsen<<m_vieActuel<<endl;
    sauveIsen<<m_etat<<endl;
    sauveIsen<<m_etatTourRestant<<endl;
    sauveIsen<<m_level;
    for(int i=0;i<4;i++) //ici impossible d'acceder aux vaiables m_nombreAttaquePossible parreil pour afficher .
    {
        sauveIsen<<endl<<m_stringNomActionPossible[i]<<endl<<m_nombreAttaquePossible[i];
    }
}
isenmon::~isenmon()
{
    //dtor
}
