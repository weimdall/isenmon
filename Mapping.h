/**
 * \file Mapping.h
 * \brief Gestion du TileMapping
 * \author J.Cassagne
 */

#ifndef MAPPING_H
#define MAPPING_H

#include <vector>
#include <fstream>
#include <SFML/Graphics.hpp>

#define MAPS "./data/maps/"

struct Tile
{
    int bloc;
    sf::Sprite sprite;
};

enum Direction
{
    HAUT, BAS, DROITE, GAUCHE, ATTEND
};
typedef enum Direction Direction;


class Mapping
{
    public:
        Mapping(sf::RenderWindow *App);
        virtual ~Mapping();
        bool TestCollision(sf::Sprite *sprite, sf::Vector2f position, Direction direction);
        bool LireFichier(std::string nomFichier);
        bool ChargerMap(std::vector <std::vector<Tile> >&calque);
        void Draw();
        sf::Vector2f getSpawn();

        int getRosolutionLargeur();
        int getResolutionHauteur();

    private:
        sf::RenderWindow *fen;
        sf::Image m_ref;
        sf::Texture m_refText;
        std::vector <std::vector<std::vector<Tile> > >multiCalques;
        std::vector <int>m_vide;
        sf::Vector2f spawn;

        int m_nbTileHauteur;
        int m_nbTileLargeur;
        int m_resolutionHauteur;
        int m_resolutionLargeur;
        int m_nbBlocsLargeur;
        int m_nbBlocsHauteur;
        int calqueCollision;
};

#endif // MAPPING_H
